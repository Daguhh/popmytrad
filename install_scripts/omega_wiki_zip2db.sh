#!/bin/bash

# Download dict from OmegaWiki project
# http://dictionarymid.sourceforge.net/dictionaries/dictsBinlingualsOmegaWiki.html

# will be erased

if [ "$1" = "-h" ] || [ -z $1 ] || [ "$1" = "--help" ]; then
    echo "Transforme un dictionnaire 'dictionarymid' en base de donnée pour PopMyTrad" 
    echo "Précisez l'archivre à traiter en arguement"
    echo ""
    echo "usage: ./omega_wiki_zip2db.sh DfM_OmegaWiki_<lang_1><lang_2>_3.5.9_08.May.2014.zi'"
    echo ""
    echo "Vous pouvez télécharger des dictionnaires sur :"
    echo "http://dictionarymid.sourceforge.net/dictionaries/dictsBinlingualsOmegaWiki.html"
    exit 0
fi


SCRIPT_PATH=$(realpath $0)
WORK_DIR="$(dirname $SCRIPT_PATH)/workdir_dico"

ARCHIVE_NAME=$(basename $1)
if ! [ "${ARCHIVE_NAME##*.}" = zip ]; then
    echo "wrong file extention, should be a zip"
    echo "run : '$(basename $0) -h' for help"
    echo "exiting..."
    exit 0
fi
ARCHIVE_PATH=$(realpath $1)
ARCHIVE_DIR=$(dirname $ARCHIVE_PATH)

LANG_1=$(echo $ARCHIVE_NAME | sed 's/DfM_OmegaWiki_\(\w\{3\}\)\w\{3\}.*/\1/g')
LANG_2=$(echo $ARCHIVE_NAME | sed 's/DfM_OmegaWiki_\w\{3\}\(\w\{3\}\).*/\1/g')

if [ $LANG_1 = $ARCHIVE_NAME ] || [ $LANG_2 = $ARCHIVE_NAME ]; then
    echo ""
    echo "Can't extract langages from file name"
    echo "Is it an Dfm/OmegaWiki zip file? Files from Dfm look like:"
    printf "\x1b[1;36m\x1b[1;40mDfM_OmegaWiki_<lang_1><lang_2>_<version>_<date>.zip\x1b[0m\x1b[0m\n"
    echo "Get one from:"
    echo "http://dictionarymid.sourceforge.net/dictionaries/dictsBinlingualsOmegaWiki.html"
    echo ""
    echo "skiping..."
    exit 0
fi

echo "Creating database for $LANG_1-$LANG_2 dictionary..."

echo "Extracting files..."
mkdir $WORK_DIR
cd $ARCHIVE_DIR
unzip  $ARCHIVE_NAME -d $WORK_DIR
cd $WORK_DIR
unzip "*.jar"  "dictionary/*.csv" -d ./
cd $WORK_DIR/dictionary


cat <<EOF > make_db.py
#/usr/bin/env python3

import os, json, sys
import re
import csv, time

LANG_1 = sys.argv[1]
LANG_2 = sys.argv[2]

try:
    import sqlite3
except:
    print(0)
    sys.exit(0)

database_file = f"../../dict_{LANG_1}_{LANG_2}_database.db"

csv_files = os.listdir()

csv_index_en = [name for name in csv_files if re.match(f"index{LANG_1}*", name)]
csv_index_fr = [name for name in csv_files if re.match(f"index{LANG_2}*", name)]
csv_directory = [name for name in csv_files if re.match("directory*", name)]

conn = sqlite3.connect(database_file)
cur = conn.cursor()

######### fill language 1 ######################
cur.execute(
    f'''CREATE TABLE
        IF NOT EXISTS index{LANG_2}
            (word TEXT,
             ref TEXT ,
             type text,
             PRIMARY KEY  (ref, type))'''
)

for f in csv_index_fr:
    cur.execute('BEGIN TRANSACTION')

    reader = csv.reader(open(f,'r'), delimiter='\t')

    for word, refs in reader:
        for ref in refs.split(','):
            dir_id, def_id, wrd_id = re.search('(\d*)-(\d*)-(\w)', ref).groups()
            ref_tot = f"{dir_id}-{def_id}"
            cur.execute(
                f'''INSERT OR IGNORE INTO index{LANG_2}
                        (word, ref, type)
                    VALUES
                        (?,?,?)''',
                (' '+word+' ', ref_tot, wrd_id)
            )

    cur.execute('COMMIT')

######### fill language 2 ######################
cur.execute(
    f'''CREATE TABLE
        IF NOT EXISTS index{LANG_1}
            (word TEXT,
             ref TEXT,
             type TEXT,
             PRIMARY KEY  (ref, type))'''
)

for f in csv_index_en:
    cur.execute('BEGIN TRANSACTION')

    reader = csv.reader(open(f,'r'), delimiter='\t')

    for word, refs in reader:
        for ref in refs.split(','):
            dir_id, def_id, wrd_id = re.search('(\d*)-(\d*)-(\w)', ref).groups()
            ref_tot = f"{dir_id}-{def_id}"
            cur.execute(
                f'''INSERT OR IGNORE INTO index{LANG_1}
                        (word, ref, type)
                    VALUES
                        (?,?,?)''',
                (' '+word+' ', ref_tot, wrd_id)
            )

    cur.execute('COMMIT')

######### fill definition ######################
cur.execute(
    f'''CREATE TABLE
        IF NOT EXISTS directory
            ({LANG_1}_wrd text,
             {LANG_1}_def text,
             {LANG_2}_wrd text,
             {LANG_2}_def text,
             ref text PRIMARY KEY)'''
)

for f in csv_directory:

    cur.execute('BEGIN TRANSACTION')

    reader = csv.reader(open(f,'r'), delimiter='\t')

    dir_id = re.search('directory(\d*).csv', f).groups()[0]
    def_id = 0

    for eng, fra in reader:
        ref_id = f"{dir_id}-{def_id}"

        eng_wrd = re.findall('^(?:\[.*\])*(.+)(?:\[.*\])*', eng) or [' ']
        eng_def = re.findall('^(?<!\[).*\[(.*)\]', eng) or ['']
        fra_wrd = re.findall('^(?:\[.*\])*(.+)(?:\[.*\])*', fra) or [' ']
        fra_def = re.findall('^(?<!\[).*\[(.*)\]', fra) or ['']

        eng_wrd = re.sub("\[.*", "", eng_wrd[0]).replace(';',', ')
        eng_def = re.sub("^\d{0,2}(\Wn){0,1}(\w*\]){0,1}", '', eng_def[0])
        fra_wrd = re.sub("\[.*", "", fra_wrd[0]).replace(';',', ')
        fra_def = re.sub("^\d{0,2}(\Wn){0,1}(\w*\]){0,1}", '', fra_def[0])

        cur.execute(
            f'''INSERT OR IGNORE INTO directory
                    ({LANG_1}_wrd, {LANG_1}_def, {LANG_2}_wrd, {LANG_2}_def, ref)
                VALUES
                    (?,?,?,?,?)''',
            (eng_wrd, eng_def, fra_wrd, fra_def, ref_id)
        )

        def_id += len(eng.encode('utf-8')) + len(fra.encode('utf-8')) + len('\n\t'.encode('utf-8'))
    cur.execute('COMMIT')

print(1)

EOF

cmd=$(python3 make_db.py $LANG_1 $LANG_2 2>&1)

echo "checking command status..."
if [ "$cmd" -eq 0 ]; then
    echo "Please install python3-sqlite3"
    exit 0 
elif [ "$cmd" -eq 1 ]; then
    echo "================="
    echo "Database is ready!"
    echo "================="
else
    echo "Something goes wrong! But what???"
fi

echo "exiting working directory..."
cd ../..

echo "cleaning extracted files..."
rm -r $WORK_DIR

echo "moving database to install path (popmytrad/dico/)"
DB_NAME="dict_${LANG_1}_${LANG_2}_database.db"
mv $DB_NAME ../dico/



