#!/bin/bash

ACTUAL_PATH=$PWD
script_path=$(dirname "${BASH_SOURCE[0]}")
SCRIPT_PATH=$(realpath $0)
SCRIPT_DIR=$(dirname $SCRIPT_PATH)
VENV_DIR="$SCRIPT_DIR/../.venv"

cd "$SCRIPT_DIR"

# Yes/No question with default
askYesNo () {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[O/n]"
                DEFAULT="o"
            else
                OPTIONS="[o/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s  -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo ${INPUT}
        if [[ "$INPUT" =~ ^[yYoO]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}

get_abs_path () {

    local file_path="$1"
    local relative_path="$2"

    if [ "${file_path:0:1}" = '/' ]; then 
        abs_path=$(realpath $file_path) 
    else 
        abs_path="$relative_path/$file_path" 
    fi

    echo "$abs_path"

}

printf "\n\x1b[1;37m\x1b[40m                 Installateur de dépendance de PopMyTrab                  \x1b[0m\x1b[0m\n"
echo ""
echo "Bienvenue dans l'intallateur de dictionnaire pour PopMyTrad"
echo "Ce script vous propose de faciliter l'installation du programme et de ses dépendances"
echo "pour les différents dictionnaires supportés"

printf "\n\x1b[1;32mVoulez vous continuer?\x1b[0m"
askYesNo "" true
if [ "$ANSWER" = false ]; then
    #exit 0
    a=1
else

    if ! [ -d "../.venv" ]; then
        echo "Creation d'un envrionnement virtuel à la racine : .venv/ "
        python3 -m venv $VENV_DIR
    fi
        
    source $VENV_DIR/bin/activate

    echo ""
    echo "Mise à jour de  pip et setuptools..."
    pip install --upgrade pip
    pip install --upgrade setuptools

    echo ""
    echo "Installation des principales dépendances..."
    pip install -r $SCRIPT_DIR/../requirements.txt

fi

echo ""
printf "\x1b[1;32mVoulez vous installer les dépendances pour les ressources en lignes?\x1b[0m"
askYesNo "" true
if [ "$ANSWER" = true ]; then
    pip install -r $SCRIPT_DIR/web_requirements.txt
else
    echo "Pour installer ultérieurement les dépendances web, excecutez dans l'envrionnement (.venv):"
    printf "\x1b[1;36m\x1b[1;40mpip install -r install_scripts/web_requirements.txt\x1b[0m\x1b[0m\n"
fi

echo ""
printf "\n\x1b[1;37m\x1b[40mInstallation d'argos-translate\x1b[0m\x1b[0m\n"
printf "\x1b[1;32mVoulez vous installer argos-translate?\x1b[0m"

IS_INSTALLED_ARGOS=0
askYesNo "" false
if [ "$ANSWER" = true ]; then
    #./install_argostranslate.sh # <--- fall back on pyqt 5.14.1 for python3.7 compatibility (debian buster)
    pip install argostranslate
    IS_INSTALLED_ARGOS=1
else
    printf "Pour installer ultérieurement argostranslate, vous pourrez utiliser la commande : "
    printf "\x1b[1;36m\x1b[1;40mpip install argostranslate\x1b[0m\x1b[0m\n"
fi

cd "$SCRIPT_DIR"

printf "\n\x1b[1;32mVous voulez vous installer des modèles pour argostranslate?\x1b[0m"
askYesNo "" false
if [ "$ANSWER" = true ]; then
    loop=1
    while [ $loop -eq 1 ]; do
        cd $ACTUAL_PATH
        read -e -p 'Entrer le chemin vers le fichier (.argosmodel): ' path_to_zip

        zip_path=$(get_abs_path $path_to_zip $ACTUAL_PATH)
        zip_name=$(basename $zip_path) 
        ext="${zip_name##*.}"

        echo $zip_name
        echo $zip_path

        cd "$SCRIPT_DIR"

        if [ "$ext" = "argosmodel" ]; then
            "$SCRIPT_DIR/install_argos_model.py" "$zip_path"
        else
            echo "Please give full path, i.e. with .argosmodel extention file name"
        fi
        
        echo ""
        printf "\x1b[1;32mSouhaitez installer un autre modèle?\x1b[0m"
        askYesNo "" false
        if [ "$ANSWER" = false ]; then
            echo ""
            echo "Vous pourrez ultérieurement installer d'autres modèles avec la commande:"
            printf "\x1b[1;36m\x1b[1;40m./install_argos_model.py <nom_archive>.argosmodel\x1b[0m\x1b[0m\n"
            loop=0
        fi
    done
    echo ""
    pyqt_version=$(cat $SCRIPT_DIR/../requirements.txt | grep PyQt)
    echo "Downgrading PyQt version to ${pyqt_version##*=}"
    pip install $pyqt_version
fi

cd "$SCRIPT_DIR"

echo ""
printf "\n\x1b[1;37m\x1b[40mInstallation d'OmegaWiki\x1b[0m\x1b[0m\n"
echo "Installation de dictionnaires OmegaWiki"
"$SCRIPT_DIR/omega_wiki_zip2db.sh" -h | sed '1,5d'

printf "\x1b[1;32mVoulez vous installer des dictionnaires OmegaWiki?\x1b[0m"
askYesNo "" false
if [ "$ANSWER" = true ]; then
    loop=1
    while [ $loop -eq 1 ]; do

        cd $ACTUAL_PATH
        read -e -p 'Entrer le chemin vers le fichier (.zip): ' path_to_zip

        zip_path=$(get_abs_path $path_to_zip $ACTUAL_PATH)
        zip_name=$(basename $zip_path) 
        ext="${zip_name##*.}"

        cd "$SCRIPT_DIR"

        if [ "$ext" = "zip" ]; then
            "$SCRIPT_DIR/omega_wiki_zip2db.sh" "$zip_path"
        else
            echo "Please give full path, i.e. with zip file name"
        fi
        
        echo ""
        printf "\x1b[1;32mSouhaitez installer un autre dictionnaire OmegaWiki?\x1b[0m"
        askYesNo "" false
        if [ "$ANSWER" = false ]; then
            echo ""
            echo "Vous pourrez ultérieurement convertir et installer des dictionnaires"
            printf "OmegaWiki via la commande : \x1b[1;36m\x1b[1;40m./omega_wiki_zip2db.sh <nom_archive>.zip\x1b[0m\x1b[0m\n"
            loop=0
        fi

    done
fi

cd "$SCRIPT_DIR"

printf "\n\x1b[1;37m\x1b[40m                        Fin de l'installation                     \x1b[0m\x1b[0m\n"
echo ""
printf "\x1b[1;32mL'intallation des dictionnaires est terminée!\x1b[0m\n"
echo ""
echo "Pour afficher la traduction/definition d'un mot, surlignez un mot et lancez:"
printf "\x1b[1;36m\x1b[1;40m./popmytrad-start.sh\x1b[0m\x1b[0m\n"
echo ""
echo "Vous pouvez aussi directement lancer la commande python,"
echo "dans ce cas, activez l'environnement virtuel avant chaque lancement:"
printf "\x1b[1;36m\x1b[1;40msource .venv/bin/activate\x1b[0m\x1b[0m\n"
echo "Puis:"
printf "\x1b[1;36m\x1b[1;40m./popmytrad-start.py\x1b[0m\x1b[0m\n"
echo ""
if [ $IS_INSTALLED_ARGOS -eq 1 ]; then
    echo ""
    echo "N'oubliez pas d'installer les modèles pour argostranslate"
    echo "Pour ajouter des modèles, télécharger les, puis lancer l'interface graphique avec :"
    printf "\x1b[1;36m\x1b[1;40margos-translate\x1b[0m\x1b[0m\n"
    echo "Enfin cliquer sur 'add_packages' et selectionez les fichiers téléchargés"
    echo ""
fi


