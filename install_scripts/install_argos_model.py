#!/usr/bin/env python3

import sys

from argostranslate import package

path = sys.argv[1]

if not path.endswith("argosmodel"):
    print("please give an .argosmodel file as argument")
    sys.exit(0)

package.install_from_path(path)

