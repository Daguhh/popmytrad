#!/usr/bin/bash

path=$(dirname $0)

if [ -d "$path/.venv" ]; then
    echo "Lancement dans l'environnement virtuel '$path/.venv/'"
    source "$path/.venv/bin/activate"
else
    echo "Pas d'environnement virtuel trouvé"
    echo "Lancement dans l'espace utilisateur..."
fi

python3 "$path/popmytrad-start.py" "$@"

