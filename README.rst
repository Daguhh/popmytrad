=========
PopMyTrad
=========

**Surlignez : traduisez!**

.. image:: https://framagit.org/Daguhh/popmytrad/-/raw/master/screenshot/in_action.png

A Propos
========

surlignez un mot/une phrase à la souris, puis lancer le script pour afficher une notification affichant le contenu de:

- `wordreference <www.wordreference.com>`_ : traductions 
- `LeRobert <https://dictionnaire.lerobert.com>`_ : définitions *(fr)*
- `LibreTranslate <https://libretranslate.com/>`_ : traductions textes
- `argos-translate <https://github.com/argosopentech/argos-translate>`_ : traduction textes en local.
- `Ẁiktionary <https://en.wiktionary.org>`_ : définitions anglaises majaritairement, définitions françaises
- `Académie française <https://www.dictionnaire-academie.fr/>`_ : définitions *(fr)* 
- `DictionaryForMIDs <http://dictionarymid.sourceforge.net/what.html>`_ / `OmegaWiki <http://www.omegawiki.org/Meta%3aMain_Page>`_ : traduction mots en local

Fonctionnalités
===============

Utilisations:

- Questionne les sources avec le mot surligné et fait apparaitre une popup.
- `Copie des résultats <https://framagit.org/Daguhh/popmytrad/-/wikis/Interface#menu-contextuel>`_ de recherche vers le presse-papier (brut, json, html)
- Fermeture automatique (perte de focus)
- `Module dico <https://framagit.org/Daguhh/popmytrad/-/wikis/Module-dico>`_ (*sources*) indépendant : utilisation "standalone" dans un `script <https://framagit.org/Daguhh/popmytrad/-/wikis/Module-dico#import-dans-un-script>`_ ou `affichage dans le terminal <https://framagit.org/Daguhh/popmytrad/-/wikis/Module-dico#exemples>`_

Interface:

- Présentation sous forme d'`onglets <https://framagit.org/Daguhh/popmytrad/-/wikis/Interface#fen%C3%AAtre-principale>`_
- Historique
- Correction/suggestion de termes
- `Panneau de configuration <https://framagit.org/Daguhh/popmytrad/-/wikis/Interface#fen%C3%AAtre-de-configuration>`_ (choix des sources, des langues)



`Voir le wiki <https://framagit.org/Daguhh/popmytrad/-/wikis/home>`_

        

