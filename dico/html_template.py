################### html templates ########################################

html_template = """<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title></title></head><body>{}</body></html>"""
TITLE = """<big>{}</big>"""
PARAGRAPH = """<p>{}</p>"""
BOLD = """<strong>{}</strong>"""
BLUE = """<center><span style="color:blue"{}</span></center>"""
ITALIC = """<em>{}</em>"""
LINK = """<a href="{}">{}</a>"""
LIST = """<ul>{}</ul>"""
LIST_ITEM = """<li>{}</li>"""
QUOTE = """<blockquote>{}</blockquote>"""


class QT:
    def __radd__(self, text):

        while text[-4:] == "<br>":
            text = text[:-4]

        return text + self.text

    def __iadd__(self, text):

        return self.__radd__(text)

    def format(self, text):

        self.text = f"""<blockquote>{text}</blockquote>"""

        return self


QUOTE = QT()


class LB:
    def __init__(self, nb=1):

        self.br = "<br>" * (nb - 1)

    def __radd__(self, text):

        if text[-8:] == "<br><br>":
            return text + self.br

        elif text[-13:] == "</blockquote>":
            return text + self.br

        else:
            return text + self.br + "<br>"

    def __iadd__(self, text):

        return self.__radd__(text)

    def __rmul__(self, nb):

        return LB(nb)


LINE_BREAK = LB()
