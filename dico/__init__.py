from .lerobert_parser import DicoRobert
from .libretranslate_parser import DicoLibreTranslate
from .wiktionary_parser import DicoWiktionary
from .wordreference_parser import DicoWordreference
from .academie_fr_parser import DicoAcademieFr
from .omega_wiki_local_db import DicoOmegaWiki
from .argostranslate_parser import DicoArgosTranslate


DICOs = [
    DicoRobert,
    DicoLibreTranslate,
    DicoWiktionary,
    DicoWordreference,
    DicoAcademieFr,
    DicoOmegaWiki,
    DicoArgosTranslate,
]
