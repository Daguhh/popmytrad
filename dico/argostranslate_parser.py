import subprocess
import requests

try:
    from argostranslate import package, translate
except ModuleNotFoundError:
    print("Please install argos-translate")

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError

LANG = {"en": "English", "fr": "French", "es": "Spanish"}


class DicoArgosTranslate(Dico):
    """ Implement LibreTranslate search """

    name = "ArgosTranslate"
    url = "https://github.com/argosopentech/argos-translate"
    category = "text" # availables : word, text

    def __init__(self, source, target):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source, target):
        """ get definition from libtranslate """

        source = LANG[source]
        target = LANG[target]

        try:
            installed_languages = translate.load_installed_languages()
        except NameError:
            raise DicoConnectionError(DicoArgosTranslate.name, "not installed")

        if not installed_languages:
            print("Argostranslate : No languages found for", source, "to", target)
            raise NoDefinitionError(word)

        source_engine = [lang for lang in installed_languages if str(lang) == source][0]
        target_engine = [lang for lang in installed_languages if str(lang) == target][0]
        translation_src_trg = source_engine.get_translation(target_engine)

        trad = translation_src_trg.translate(word)

        datas = {"traduction": [{"t_wrd": trad}]}

        return datas


if __name__ == "__main__":

    DicoArgosTranslate.cmd_line_tool()
