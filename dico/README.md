# Utilisation en mode "script" du module dico

Ces fichiers définissent les sources de traduction pour PopMyTrad, cependant, vous pouvez les éxectuer individuellements:

- sous forme de commande pour directement pour effectuer des recherches dans un terminal.
- dans vos propres scripts python


## En commande

### Usage

Chacun des scripts dans le dossier *dico/* peut être exécuté via la commande:

	usage: python3 dictionnaire.py [-h] [--json] [--html] word source target

	positional arguments:
		word
		source
		target

	optional arguments:
	    -h, --help      show this help message and exit
		--json      force la sortie au format json
		--html      force la sortie au format html

Par défaut la sortie est adaptée à un affichage en terminal (coloration)

### Exemples

Consulter directement le dictionnaire OmegaWiki dans un terminal (+coloration)

```bash
python3 omega_wiki_local_db.py myrtille fr en
```
![myrtille](https://framagit.org/Daguhh/popmytrad/-/raw/master/screenshot/retour_terminal.png)

Obtenir la recherche OmegaWiki au format json:

```bash
python3 omega_wiki_local_db.py myrtille fr en --json
```

```
{
    "definitions": [
        {
            "s_wrd": " myrtille",
            "s_def": "Fruit bleu fonc\u00e9 comestible d'une plante du genre Vaccinium, mesurant jusqu'\u00e0 10mm de diam\u00e8tre.",
            "t_wrd": " blueberry, huckleberry",
            "t_def": "Dark-blue, round edible fruit of a plant in the genus Vaccinium, up to approximately 10 mm diameter."
        }
    ]
}
```

## Import dans un script

```python
>>> from dico import DicoNomDuDico
>>> datas = DicoNomDuDico.get_datas(word, source, target)
>>> html_text = DicoNomDuDico.get_html(word, datas)
```
 (Ouvrez le fichier *dico/__init__.py* pour obtenir les différents nom de classes
