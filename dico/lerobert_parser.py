import sys
import requests
from bs4 import BeautifulSoup

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError

try:
    import unidecode
except ImportError as e:
    print("please install unidecode module to deal with accent")


class DicoRobert(Dico):
    """Implement LeRobert online dictionnary"""

    name = "LeRobert"
    url = "https://dictionnaire.lerobert.com/"
    category = "word" # availables : word, text

    def __init__(self, source="fr", target=None):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source=None, target=None):
        """parse online word definition of dictionnary LeRobert

        Parameters
        ----------
            word : str
                mot à traduire
        Returns
        -------
            datas : dict
                parsed element from dictionary

                datas structure ::

                    {
                        'definitions' :
                            [ <definition 1>, <definition 2>, ... ],
                        'synonymes' :
                            [<synonyme 1>, ..., ],
                        'examples' :
                            [
                                {
                                    'citation' : <citation 1>,
                                    'author' : <author 1>,
                                },
                                ...
                            ]
                    }
        """

        if len(word.split(' ')) > 1:
            print("Please select only one word with this dictionary")
            raise NoDefinitionError(word)

        if "unidecode" in sys.modules:
            word = unidecode.unidecode(word)

        if source != "fr":
            print(
                "dictionary 'LeRobert' support only french, searching for '"
                + word
                + "' in french"
            )

        url = "https://dictionnaire.lerobert.com/definition/" + word

        try:
            text = requests.get(url=url, timeout=5).text
        except requests.exceptions.ConnectTimeout as e:
            raise DicoConnectionError(DicoRobert.name, "timeout")
        except requests.exceptions.ConnectionError as e:
            raise DicoConnectionError(DicoRobert.name, "no connection")
        except:
            raise DicoConnectionError(DicoRobert.name)

        soup = BeautifulSoup(text, "html.parser")

        datas = {}

        datas["definitions"] = []
        for definition in soup.find_all("span", "d_dfn"):
            datas["definitions"] += [{"t_wrd": definition.text}]

        datas["synonymes"] = []
        for synonyme in soup.find_all("span", "s_syni"):
            datas["synonymes"] += [{"t_wrd": synonyme.text}]

        datas["examples"] = []
        for example in soup.find_all("div", "ex_example"):
            author = example.a.extract().text
            citation = example.text
            datas["examples"] += [{"s_def": citation, "t_def": author}]

        if len(datas["definitions"]) == 0:
            raise NoDefinitionError(word)

        return datas


if __name__ == "__main__":

    DicoRobert.cmd_line_tool()
