import sys
import requests

from collections import deque
from itertools import islice

from bs4 import BeautifulSoup
import bs4

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
    from html_template import *
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError
    from .html_template import *


def read_2_by_2(iterable, size=2):
    iterable = iterable + [" "]
    d = deque(islice(iterable, size), size)
    count = 0
    for x in iterable:
        count += 1
        d.append(x)
        if count == 2:
            count = 0
            yield d


class DicoAcademieFr(Dico):
    """Implement LeRobert online dictionnary"""

    name = "Academie_fr"
    url = "https://www.dictionnaire-academie.fr"
    category = "word" # availables : word, text

    def __init__(self, source="fr", target=None):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source=None, target=None):
        """parse online word definition of dictionnary LeRobert

        Parameters
        ----------
            word : str
                mot à traduire
        Returns
        -------
            datas : dict
                parsed element from dictionary

                datas structure ::

                    {
                        'definitions' :
                            [ <definition 1>, <definition 2>, ... ],
                        'synonymes' :
                            [<synonyme 1>, ..., ],
                        'examples' :
                            [
                                {
                                    'citation' : <citation 1>,
                                    'author' : <author 1>,
                                },
                                ...
                            ]
                    }
        """

        if len(word.split(' ')) > 1:
            print("Please select only one word with this dictionary")
            raise NoDefinitionError(word)

        if source != "fr":
            print(
                "dictionary 'Academie_fr' support only french, searching for '"
                + word
                + "' in french"
            )

        try:
            r = requests.post(
                "https://www.dictionnaire-academie.fr/search",
                data={"term": word},
                timeout=5,
                verify=False
            )
        except requests.exceptions.ConnectTimeout as e:
            raise DicoConnectionError(DicoAcademieFr.name, "timeout")
        except requests.exceptions.ConnectionError as e:
            raise DicoConnectionError(DicoAcademieFr.name, "no connection")
        except:
            raise DicoConnectionError(DicoAcademieFr.name)


        try:
            article_url = r.url.replace("search", "")
            r=requests.get(article_url, verify=False)
            soup = BeautifulSoup(r.text)
            blocs = soup.find_all("div", "blocSection")

            IS_BLOC_SECTION = True
            if len(blocs) == 0:
                blocs = soup.find_all("div", "s_Article")
                IS_BLOC_SECTION = False

            dct = {}
            dct_cat = {}
            for bloc in blocs:

                # print(bloc)
                if IS_BLOC_SECTION:
                    titre = bloc.find_all("div", "titreBlocSection")[0]
                    try:
                        cat = titre.find_all("span", "s_cat")[0].extract()
                        conj = titre.find_all("span", "s_ConjPlur")[0].extract()
                    except:
                        cat = BeautifulSoup(" ")
                        conj = BeautifulSoup(" ")

                    dct_cat[cat.text] = {"id": titre.text, "conj": conj.text}
                    items = bloc.find_all("div", "s_DivRom")
                else:
                    cat = bloc.find_all("span", "s_cat")[0].extract()
                    dct_cat[cat.text] = {}
                    items = blocs  # .find_all("div", "s_DivNum")

                dct_cat[cat.text]["sens"] = []
                for item in items:

                    for element in item:
                        dct_meta = {}
                        if isinstance(element, bs4.element.Tag):
                            meta = element.find_all("span", "s_Meta")

                            if len(meta) != 0:
                                dct_meta["meta"] = meta[0].extract().text

                            text = []
                            for elt_1, elt_2 in read_2_by_2(list(element)):
                                for elt in [elt_1, elt_2]:

                                    if isinstance(elt, bs4.element.Tag):
                                        text.append({"i": elt.text})
                                    else:
                                        text.append({"s": elt})
                            dct_meta["sens"] = text

                        dct_cat[cat.text]["sens"] += [dct_meta]
        except IndexError as e:
            print(e)
            print(
                "Error while parsing "
                + DicoAcademieFr.name
                + "\n Maybe page was no founded"
            )
            print(
                "check url :",
                r.url,
                "if its not empty, there's an issue with the parser",
            )
            dct_cat = []
            raise e

        datas = dct_cat
        if len(datas) == 0:
            raise NoDefinitionError(word)

        return datas

    def get_html(self, word, datas):

        if "Error" in datas.keys():
            return super().get_html(word, datas)

        html = TITLE.format(word)

        for cat in datas.keys():
            html += PARAGRAPH.format(BOLD.format(cat))

            lst = ""
            if "sens" in datas[cat].keys():
                for sens in datas[cat]["sens"]:

                    paragraph = ""

                    if "meta" in sens.keys():
                        paragraph += BOLD.format(sens["meta"])

                    if "sens" in sens.keys():
                        for item in sens["sens"]:
                            for form, txt in item.items():
                                if form == "i":
                                    paragraph += ITALIC.format(txt)
                                elif form == "s":
                                    paragraph += txt

                    lst += LIST_ITEM.format(paragraph)
            html += LIST.format(lst)
        html += self.get_ref()

        return html


if __name__ == "__main__":

    DicoAcademieFr.cmd_line_tool()
