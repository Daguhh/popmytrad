#!/usr/bin/env python3

"""
Local dictionary thanks to DictionaryForMIDs_ and OmegaWiki_
(Distributed under "Creative Commons Attribution-ShareAlike 3.0 Unported License")

Download language pack from `<http://dictionarymid.sourceforge.net/dictionaries/dictsBinlingualsOmegaWiki.html>`_
Then convert the zip file into sqlite3 database using omega_wiki_zip2db.sh script;:

    ./omega_wiki_zip2db.sh DfM_OmegaWiki_{lang_1}{lang_2}_3.5.9_08.May.2014.zip


.. _OmegaWiki: http://www.omegawiki.org/Meta%3aMain_Page
.. _DictionaryForMIDs: http://dictionarymid.sourceforge.net/what.html


"""

import os, sys
from itertools import chain
import re
import sqlite3

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError
else:
    from .dico_base import Dico, NoDefinitionError


try:
    import unidecode
except ImportError as e:
    print("please install unidecode module to deal with accent")

##########  User parameters  #############

# Directory where OmegaWiki databases belong
#  by default is same place a this script,
DB_DIR = os.path.dirname(os.path.abspath(__file__))

# ISO language naming convention convertion table (ISO-639-1 to ISO-639-2)
# Convert 2 letter keycode into the OmegaWiki ones,
# please update this list to add a language
LANG = {
    "af": "Afr",
    "??": "Ast",
    "be": "Bel",
    "?2": "Bre",
    "bg": "Bul",
    "ca": "Cat",
    "cs": "Ces",
    "cy": "Cym",
    "da": "Dan",
    "de": "Deu",
    "el": "Ell",
    "en": "Eng",
    "fr": "Fra",
    "gd": "Gla",
    "ga": "Gle",
    "hi": "Hin",
    "hr": "Hrv",
    "hu": "Hun",
    "is": "Isl",
    "it": "Ita",
    "ja": "Jpn",
    "la": "Lat",
    "lt": "Lit",
    "mk": "Mkd",
    "nl": "Nld",
    "oc": "Oci",
    "pl": "Pol",
    "pt": "Por",
    "ro": "Ron",
    "ru": "Rus",
    "?4": "San",
    "sk": "Slk",
    "es": "Spa",
    "sw": "Swa",
    "sv": "Swe",
    "?5": "Swh",
    "tr": "Tur",
    "uk": "Ukr",
    "?1": "Arb",
}

# Database name format (filled with LANG values)
DATABASE = "dict_{}_{}_database.db"


#########  Begin class  ##################


class DicoOmegaWiki(Dico):
    """Implement OmegaWiki local dictionnary"""

    name = "OmegaWiki"
    url = "http://dictionarymid.sourceforge.net/dictionaries/dictsBinlingualsOmegaWiki.html"
    category = "word" # availables : word, text

    def __init__(self, source="fr", target=None):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source=None, target=None):

        # Check language key
        try:
            source = LANG[source]
            target = LANG[target]
        except KeyError as e:
            print(f"No equivalent keycode for {source} or {target}")
            print(f"Please edit {__file__} and add the wanted language keycode")
            raise NoDefinitionError(word)

        # Try load database
        database = os.path.join(DB_DIR, DATABASE.format(source, target))

        if not os.path.exists(database):
            database = os.path.join(DB_DIR, DATABASE.format(target, source))
        if not os.path.exists(database):
            print(f"No dictionary database for {source}-{target}")
            raise NoDefinitionError(word)

        if "unidecode" in sys.modules:
            word = unidecode.unidecode(word)

        DB_PATH = os.path.join(DB_DIR, database)

        # Resquet def from database
        conn = sqlite3.connect(DB_PATH)
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute(
            f"""SELECT d.{source}_wrd, d.{source}_def, d.{target}_wrd, d.{target}_def
                FROM index{source} s, directory d
                WHERE s.word
                LIKE
                        ('% ' || ? || ' %')
                    AND
                        s.ref = d.ref;""",
            (word,),
        )

        result = cur.fetchall() or [("", "", "", "")]
        conn.close()

        # format datas for display
        datas = {}
        datas["definitions"] = []
        for i in range(len(result)):
            s_wrd, s_def, t_wrd, t_def = result[i]

            if not re.search("\w", s_wrd):
                s_wrd = word

            datas["definitions"] += [
                {
                    "s_wrd": s_wrd,
                    "s_def": s_def,
                    "t_wrd": t_wrd,
                    "t_def": t_def,
                }
            ]

        return datas


if __name__ == "__main__":

    DicoOmegaWiki.cmd_line_tool()
