import requests

from wiktionaryparser import WiktionaryParser

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError


class DicoWiktionary(Dico):
    """ Implement LibreTranslate search """

    name = "Wiktionary"
    url = "https://fr.wiktionary.org"
    category = "word" # availables : word, text

    LANG_TABLE = {
        "fr": "french",
        "en": "english",
    }

    def __init__(self, source, target):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source, target=None):
        """ get definition from wiktionary """

        if len(word.split(' ')) > 1:
            print("Please select only one word with this dictionary")
            raise NoDefinitionError(word)

        try:
            source = DicoWiktionary.LANG_TABLE[source]
        except KeyError as e:
            print(
                "This source is not defined, please edit 'LANG_TABLE' in '/dico/wiktionay_parser.py' to add a language"
            )

        try:
            parser = WiktionaryParser()
            # word = parser.fetch('test')
            datas = parser.fetch(word, source)
        except requests.exceptions.ConnectTimeout as e:
            raise DicoConnectionError(DicoWiktionary.name, "timeout")
        except requests.exceptions.ConnectionError as e:
            raise DicoConnectionError(DicoWiktionary.name, "no connection")
        except:
            raise DicoConnectionError(DicoWiktionary.name)

        if len(datas) == 0 or len(datas[0]["definitions"]) == 0:
            raise NoDefinitionError(word)

        datas = datas[0]["definitions"][0]
        datas.pop("partOfSpeech")

        dct = {}
        dct["definition"] = []
        dct["relatedWords"] = []
        dct["examples"] = []

        for definition in datas["text"]:
            dct["definition"] += [{"s_wrd": definition}]
        for relword in datas["relatedWords"]:
            dct["relatedWords"] += [{"s_wrd": relword}]
        for example in datas["examples"]:
            dct["examples"] += [{"s_def": example}]

        # parser.set_default_language('french')
        # parser.exclude_part_of_speech('noun')
        # parser.include_relation('alternative forms')

        return dct


if __name__ == "__main__":

    DicoWiktionary.cmd_line_tool()
