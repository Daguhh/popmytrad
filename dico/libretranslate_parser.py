import subprocess
import requests

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError


class DicoLibreTranslate(Dico):
    """ Implement LibreTranslate search """

    name = "LibreTranslate"
    url = "https://libretranslate.com/"
    category = "text" # availables : word, text

    def __init__(self, source, target):

        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source, target):
        """ get definition from libtranslate """

        # p = subprocess.Popen(("http", "https://libretranslate.com/translate source=fr target=en q='" + word + "'"), stdout=subprocess.PIPE)
        # datas = subprocess.check_output(("jq", ".translatedText"), stdin=p.stdout)

        # cmd = "https --ignore-stdin https://libretranslate.com/translate source=" + source + " target=" + target + " q='" + word + "' | jq .translatedText"
        # traduction = subprocess.check_output(cmd, shell=True).decode('utf-8')
        if len(word.split(' ')) > 1:
            print("Please select only one word with this dictionary")
            raise NoDefinitionError(word)

        data = {"source": source, "target": target, "q": f"'{word}'"}
        try:
            r = requests.post(
                "https://libretranslate.com/translate", data=data, timeout=5
            )
        except requests.exceptions.ConnectTimeout as e:
            raise DicoConnectionError(DicoLibreTranslate.name, "timeout")
        except requests.exceptions.ConnectionError as e:
            raise DicoConnectionError(DicoLibreTranslate.name, "no connection")
        except:
            raise DicoConnectionError(DicoLibreTranslate.name)

        if 'error' in r.json():
            raise DicoConnectionError(DicoLibreTranslate.name, r.json()['error'])

        try:
            traduction = r.json()["translatedText"].strip("'")
        except KeyError as e:
            print(e)
            raise NoDefinitionError(word)

        datas = {"traduction": [{"s_wrd": traduction}]}

        return datas


if __name__ == "__main__":

    DicoLibreTranslate.cmd_line_tool()
