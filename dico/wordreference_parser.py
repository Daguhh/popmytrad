#!/usr/bin/env python3


from itertools import chain, zip_longest

import requests
from bs4 import BeautifulSoup

if __name__ == "__main__":
    from dico_base import Dico, NoDefinitionError, DicoConnectionError
else:
    from .dico_base import Dico, NoDefinitionError, DicoConnectionError


class DicoWordreference(Dico):
    """Implement wordreference online dictionnary"""

    name = "wordreference"
    url = "https://www.wordreference.com"
    category = "word" # availables : word, text

    def __init__(self, source, target):
        super().__init__(source, target)

    @staticmethod
    def get_datas(word, source, target):
        """Parse definition on online dictionnary wordreference

        Parameters
        ----------
            word : str
                mot à traduire
            lang_orig : str
                identifiant de la langue d'origine
            lang_dest : str
                identifiant de la langue de destination
        Returns
        -------
            datas : dict
                parsed element from dictionary

                datas structure ::

                    {
                        'definitions' :
                            [ <definition 1>, <definition 2> ]
                    }
        """

        datas = DicoWordreference.get_all_datas(word, source, target)

        return datas

    @staticmethod
    def get_all_datas(word, source, target):

        if len(word.split(' ')) > 1:
            print("Please select only one word with this dictionary")
            raise NoDefinitionError(word)

        url = "https://www.wordreference.com/" + source + target + "/" + word

        try:
            text = requests.get(url=url, timeout=5).text
        except requests.exceptions.ConnectTimeout as e:
            raise DicoConnectionError(DicoWordreference.name, "timeout")
        except requests.exceptions.ConnectionError as e:
            raise DicoConnectionError(DicoWordreference.name, "no connection")
        except:
            raise DicoConnectionError(DicoWordreference.name)

        soup = BeautifulSoup(text, "html.parser")

        sections = soup.find_all("table", "WRD")

        try:

            header = list(sections[0].find_all("tr", "langHeader")[0].children)

            s_l, t_l = (x["class"] for x in [header[0], header[2]])
            s_l, t_l = s_l[0], t_l[0]

            word_dct = {}
            for section in sections:

                section_name = section.find_all("tr", "wrtopsection")[0].text

                definitions = []
                definition = []

                prev = "None"

                # group by definitions
                for subsec in section.find_all("tr"):
                    if subsec.attrs["class"] != prev:

                        prev = subsec.attrs["class"]

                        definitions += [definition]
                        definition = []

                    definition += subsec
                definitions += [definition]

                # for each def get source word, target word, source example, target example
                lst = []
                for d in definitions:
                    dct = {}
                    for s in d:
                        try:
                            s.em.extract()
                        except AttributeError as e:
                            pass
                        try:
                            s_cls = s.attrs["class"][0]
                            dct[s_cls] = s.text
                        except KeyError:
                            pass

                    try:
                        dct["s_wrd"] = dct.pop(s_l)
                    except KeyError:
                        pass
                    try:
                        dct["t_wrd"] = dct.pop(t_l)
                    except KeyError:
                        pass
                    try:
                        dct["s_def"] = dct.pop(s_l[:2] + "Ex")
                    except KeyError:
                        pass
                    try:
                        dct["t_def"] = dct.pop(t_l[:2] + "Ex")
                    except KeyError:
                        pass

                    if dct:
                        lst += [dct]

                lst = lst[1:]
                word_dct[section_name] = lst

        except IndexError:
            raise NoDefinitionError(word)

        return word_dct


if __name__ == "__main__":

    DicoWordreference.cmd_line_tool()
