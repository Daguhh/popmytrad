import sys

try:
    from .html_template import *
except:
    from html_template import *


class NoDefinitionError(Exception):
    """ Handle no online definition of searched word """

    def __init__(self, word):
        self.message = "le mot " + word + " n'a pas été trouvé"
        self.word = word
        super().__init__(self.message)


class DicoConnectionError(Exception):
    """ Handle no online definition of searched word """

    def __init__(self, dico_name, error_type="unknown"):
        self.message = f"{dico_name} : {error_type}"
        self.dico_name = dico_name
        self.error_type = error_type
        super().__init__(self.message)


class DicoError(Exception):
    """ Handle all others exceptions """

    def __init__(self, dico_name, error_type="unknown"):
        self.message = f"{dico_name} : {error_type}"
        self.dico_name = dico_name
        self.error_type = error_type
        super().__init__(self.message)


class Dico:
    """ A base class for dictionnary """

    def __init__(self, source, target):

        super(Dico, self).__init__()
        self.source = source
        self.target = target
        self.datas = ""

    def translate(self, word):
        """translate and generate html"""

        try:
            datas = self.get_datas(word, self.source, self.target)
        except NoDefinitionError as e:
            print(e)
            datas = {"Error": [{"s_wrd": 1}], "non trouvé": [{"s_wrd": word}]}
        except DicoConnectionError as e:
            print(e)
            datas = {
                "Error": [
                    {"s_wrd": 2},
                    {"s_wrd": f"{e.dico_name} : {e.error_type}"},
                    {"t_wrd": f"{word} : pas de définition trouvée"},
                ]
            }

        self.datas = datas

    @classmethod
    def get_html(cls, word, datas):
        """Build html text"""

        html = TITLE.format(word) + LINE_BREAK

        for section in datas:
            html += BLUE.format(BOLD.format(section)) + LINE_BREAK
            for trad in datas[section]:
                try:
                    html += BOLD.format(trad["s_wrd"]) + LINE_BREAK
                except KeyError:
                    pass
                try:
                    html += trad["t_wrd"] + LINE_BREAK
                except KeyError:
                    pass
                try:
                    html += QUOTE.format(trad["s_def"])
                except KeyError:
                    pass
                try:
                    html += QUOTE.format(trad["t_def"])
                except KeyError:
                    html += LINE_BREAK

        html += cls.get_ref()

        return html

    @classmethod
    def get_ref(cls):
        """Make ref for online engine"""

        return "Propulsé par " + LINK.format(cls.url, cls.name)

    @classmethod
    def cmd_line_tool(cls):

        import argparse

        parser = argparse.ArgumentParser()
        parser.add_argument("word")
        parser.add_argument("source")
        parser.add_argument("target")
        parser.add_argument("--json", action="store_true")
        parser.add_argument("--html", action="store_true")

        args, unknown = parser.parse_known_args()
        datas = cls.get_datas(args.word, args.source, args.target)

        if not (args.json or args.html):

            cls.shell_color_print(datas)

        elif args.json:

            import json

            print(json.dumps(datas, indent=4))

        elif args.html:
            print(cls.get_html("", datas))

    @staticmethod
    def shell_color_print(datas):

        import sys
        from functools import reduce

        def resize_string(sentence, sep=" ", length=70):

            return reduce(
                lambda x, p: x + [p]
                if len(x[-1].rsplit("\n")[-1]) + len(p) > length
                else x[:-1] + [x[-1] + p + sep],
                sentence.split(sep),
                [""],
            )

        TERM_SECTION = "\x1b[1;37m\x1b[40m{:^70}\x1b[0m\x1b[0m"
        TERM_HIGHLIGHT_1 = "\x1b[1;36m\x1b[1;40m{}\x1b[0m\x1b[0m\n"
        TERM_HIGHLIGHT_2 = "\x1b[1;32m{}\x1b[0m\n"

        print()
        for section, content in datas.items():

            sys.stdout.write(TERM_SECTION.format(section))
            print("\n")

            for item in content:

                if "s_wrd" in item.keys():

                    sys.stdout.write(TERM_HIGHLIGHT_1.format(item["s_wrd"].strip()))

                if "t_wrd" in item.keys():

                    sys.stdout.write(TERM_HIGHLIGHT_2.format(item["t_wrd"].strip()))

                if "s_def" in item.keys():

                    for line in resize_string(item["s_def"]):
                        print("\t{}".format(line))
                    print()

                if "t_def" in item.keys():

                    for line in resize_string(item["t_def"]):
                        print("\t{}".format(line))
                    print()
