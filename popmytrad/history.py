import json
import sys
from time import time
from pathlib import Path

import sqlite3
from sqlite3 import Error


DATABASE_DIR = Path.home() / ".popmytrad"
if not DATABASE_DIR.exists():
    Path.mkdir(DATABASE_DIR)

DATABASE_PATH = str(DATABASE_DIR / "dict_database.db")


class SqliteHistory:
    def __init__(self, source, target, word):

        self.source = source
        self.target = target
        self.word = word

        self.con = self.sql_connection()
        self.sql_table()

    def __enter__(self):

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):

        self.con.close()

    def sql_connection(self):

        try:
            con = sqlite3.connect(DATABASE_PATH)
            return con

        except Error as e:
            print(e)

    def disconnect(self):

        self.con.close()

    def sql_table(self):

        cursorObj = self.con.cursor()
        cursorObj.execute(
            """create table if not exists dict_history
                    (word text,
                     dictionary text,
                     source text,
                     target text,
                     datas json,
                     t_access real)"""
        )
        self.con.commit()

    def add(self, dictionary, datas):

        print(f"Adding '{self.word}' to search history database...")

        cursorObj = self.con.cursor()

        cursorObj.execute(
            """INSERT INTO dict_history
                    (word,
                     dictionary,
                     source,
                     target,
                     datas,
                     t_access)
               VALUES
                    (?, ?, ?, ?, ?, ?)""",
            (self.word, dictionary, self.source, self.target, json.dumps(datas), time()),
        )

        self.con.commit()

    def update(self, dictionary, datas):

        print(f"Updating '{self.word}' definitions in history database")
        cursorObj = self.con.cursor()

        cursorObj.execute(
            """UPDATE dict_history
               SET datas=?, t_access=?
               WHERE word=?
                    AND
                        dictionary=?
                    AND
                        source=?
                    AND
                        target=?""",
            (json.dumps(datas), time(), self.word, dictionary, self.source, self.target),
        )

        self.con.commit()

    def get_word_list(self, sort_criteria='Access Time', sort_order=' Asc'):
        # sort_criteria = [time | alphabetic]
        # sort_order = [ASC | DESC]

        sort_criteria = {
            'Access Time' : 't_access',
            'Alphabetic' : 'word'
        }[sort_criteria]

        sort_order = {
            ' Asc' : 'ASC',
            ' Desc' : 'DESC'
        }[sort_order]

        cursorObj = self.con.cursor()

        cursorObj.execute(
            f"""SELECT DISTINCT word
               FROM dict_history
               WHERE
                    source=?
               AND
                    target=?
               ORDER BY {sort_criteria} {sort_order};""",
            (self.source, self.target),
        )

        words = cursorObj.fetchall()
        return [w[0] for w in words]

    def exist_in(self, website):

        cursorObj = self.con.cursor()

        cursorObj.execute(
            """SELECT EXISTS
                    (SELECT 1
                     FROM dict_history
                     WHERE
                        word=?
                     AND
                        dictionary=?
                     AND
                        source=?
                     AND
                        target=?
                     LIMIT 1)""",
            (self.word, website, self.source, self.target),
        )

        record = cursorObj.fetchone()
        return bool(record[0])

    def get_datas_from(self, website):

        print(f"Search '{self.word}' in history...")
        cursorObj = self.con.cursor()

        cursorObj.execute(
            f"""SELECT datas
                FROM dict_history
                WHERE
                    word=?
                AND
                    dictionary=?
                AND
                    source=?
                AND
                    target=?""",
            (self.word, website, self.source, self.target),
        )

        datas = cursorObj.fetchall()

        return json.loads(datas[0][0])
