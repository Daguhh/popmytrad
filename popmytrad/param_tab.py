import sys

from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import Qt, QModelIndex, QItemSelectionModel, QTimer

from PyQt5.QtWidgets import (
    QMainWindow,
    QApplication,
    QListView,
    QAbstractItemView,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLineEdit,
    QPushButton,
    QLabel,
    QFrame,
    QGroupBox,
    QRadioButton,
    QCheckBox,
    QTabWidget,
)


class ParamTabs(QTabWidget):

    def __init__(self, save_session):

        super().__init__()

        self.save = save_session

        self.tabs = {
            'Dictionnary' : ChooseTabWdg(),
            'Modules' : ChooseModulesWdg(),
            'Window' : WindowConfTab(),
        }

        for name, tab in self.tabs.items():
            self.addTab(tab, name)

        self.set_states()

    def get_states(self):

        save = {}

        for name, tab in self.tabs.items():
            save[name] = tab.get_states()

        return save

    def set_states(self):

        for name, tab in self.tabs.items():
            tab.set_states(self.save[name])


class ChooseTabWdg(QWidget):

    def __init__(self):
        super().__init__()

        vbox = QVBoxLayout()

        self.auto_detect_lang_btn = QCheckBox('auto detect language')
        self.auto_detect_lang_btn.setDisabled(True)

        lang_select_box = QHBoxLayout()

        self.switch_lang_Btn = QPushButton("Switch")
        self.switch_lang_Btn.clicked.connect(self.switch_lang_Act)
        self.sourceEdit = QLineEdit(self)
        # self.sourceEdit.setPlaceholderText("source")
        self.sourceEdit.textEdited.connect(self.new_lang_Act)
        self.sourceEdit.setToolTip("ISO-639-1 (2 characters)")
        self.targetEdit = QLineEdit(self)
        # self.targetEdit.setPlaceholderText("target")
        self.targetEdit.textEdited.connect(self.new_lang_Act)
        self.targetEdit.setToolTip("ISO-639-1 (2 characters)")

        #self.set_lang_btn.clicked.connect(self.showlangedit)
        self.auto_detect_lang_btn.clicked.connect(self.showlangedit)

        lang_select_box.addWidget(self.switch_lang_Btn)
        lang_select_box.addWidget(self.sourceEdit)
        lang_select_box.addWidget(QLabel(">"))
        lang_select_box.addWidget(self.targetEdit)

        vbox.addWidget(self.auto_detect_lang_btn)
        vbox.addLayout(lang_select_box)

        self.dct_list = OrderNChooseWdg()
        vbox.addWidget(self.dct_list)

        self.setLayout(vbox)

    def showlangedit(self):

        is_auto_detect = self.auto_detect_lang_btn.isChecked()

        self.switch_lang_Btn.setDisabled(is_auto_detect)
        self.sourceEdit.setDisabled(is_auto_detect)

    def set_states(self, save):

        self.sourceEdit.setText(save['source'])
        self.targetEdit.setText(save['target'])

        dico_sorted = dict(sorted(
            save['dicos'].items(),
            key=lambda e:int(e[1]),
            reverse=True
        ))

        for dict_name, state in dico_sorted.items():
            self.dct_list.add_item(dict_name, int(state))

    def get_states(self):

        return {
            'source' : self.sourceEdit.text(),
            'target' : self.targetEdit.text(),
            "dicos" : self.dct_list.get_states()
        }

    def switch_lang_Act(self, e):

        if self.switch_lang_Btn.text() == "Switch":
            target = self.sourceEdit.text()
            source = self.targetEdit.text()

            self.sourceEdit.setText(source)
            self.targetEdit.setText(target)

        else:
            # Apply
            source = self.sourceEdit.text()
            target = self.targetEdit.text()

            self.switch_lang_Btn.setText("Switch")

        return {"source": source, "target": target}


    def new_lang_Act(self, e):

        self.switch_lang_Btn.setText("Apply")


class OrderNChooseWdg(QListView):

    def __init__(self):
        super().__init__()

        self.setDragDropMode(QAbstractItemView.InternalMove)
        self.setDefaultDropAction(Qt.MoveAction)
        self.setDragDropOverwriteMode(False)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setDragEnabled(True)

        self.old_row = -1000
        self.is_shift_pressed = False
        self.item_list = []

        self.model = QStandardItemModel(self)
        self.model.rowsInserted.connect(self.get_inserted_row)
        self.model.dataChanged.connect(self.get_states)

        self.setModel(self.model)
        self.setSelectionMode(QAbstractItemView.SingleSelection)

    def keyPressEvent(self, e):

        if e.key() == Qt.Key_Shift:

            self.is_shift_pressed = True

        # Move selected QStandardItem up or down in QListView
        indexes = self.selectionModel().selectedIndexes()
        if indexes:
            row = indexes[0].row()

            if self.is_shift_pressed:

                if e.key() == Qt.Key_Up:

                    if row > 0:
                        item = self.model.takeRow(row)
                        self.model.insertRow(row - 1, item)
                        self.new_row = row - 1
                        QTimer.singleShot(1, self.update_selection)

                elif e.key() == Qt.Key_Down:

                    if row < self.model.rowCount() - 1:
                        item = self.model.takeRow(row)
                        self.model.insertRow(row + 1, item)
                        self.new_row = row + 1
                        QTimer.singleShot(1, self.update_selection)

        QListView.keyPressEvent(self, e)

    def keyReleaseEvent(self, e):

        if e.key() == Qt.Key_Shift:
            self.is_shift_pressed = False

    def get_states(self):

        dicos = {}

        for i in range(self.model.rowCount()):
            name = self.model.item(i).text()
            state = self.model.item(i).checkState()

            dicos[name] = state

        return dicos

    def get_inserted_row(self, parent, row_inserted, row_end):

        self.new_row = row_inserted  # save index to update selection

        if self.old_row < self.new_row:
            self.new_row += -1  # don't count current item, it will be removed

        # Give delay after item changed to get right row value
        QTimer.singleShot(1, self.get_states)
        QTimer.singleShot(1, self.update_selection)

    def mousePressEvent(self, e):
        """Trigger 'set_old_row' with a small delay so new selection is effective"""

        # Save index (after small delay) in case of drag'n'drop
        QTimer.singleShot(1, self.set_old_row)

        QListView.mousePressEvent(self, e)

    def set_old_row(self):
        """Save currently selected item row (on mouse event) before draging it"""

        indexes = self.selectionModel().selectedIndexes()
        if indexes:
            self.old_row = indexes[0].row()

    def update_selection(self):
        """Highlight item that have been moved given its new row"""

        index = self.model.index(self.new_row, 0)
        self.selectionModel().setCurrentIndex(index, QItemSelectionModel.ClearAndSelect)
        self.scrollTo(index)

    def add_item(self, name, state):

        item = QStandardItem(name)

        item.setCheckable(True)
        item.setCheckState(state)
        item.setDragEnabled(True)
        item.setDropEnabled(False)
        item.setToolTip("Drag and drop or 'shift + arrows' to change tab order")

        self.model.appendRow(item)
        self.item_list += [{name: item}]


class ChooseModulesWdg(QWidget):

    SESSION_SAVE = {}

    def __init__(self):
        super().__init__()

        vbox = QVBoxLayout()

        spell_check_grp= QGroupBox("Spelling Tab")
        #spell_check_grp.setCheckable(True)
        spell_check_box = QVBoxLayout()

        spell_check_btn = QRadioButton('Check')
        spell_check_btn.setToolTip("Give suggestion with aspell")
        auto_correct_btn = QRadioButton('Auto correct')
        auto_correct_btn.setToolTip("Auto correctwith aspell")

        spell_check_box.addWidget(spell_check_btn)
        spell_check_box.addWidget(auto_correct_btn)
        spell_check_grp.setLayout(spell_check_box)

        history_grp = QGroupBox("History Tab")
        history_box = QVBoxLayout()
        history_overide_btn = QCheckBox('Override')
        history_overide_btn.setToolTip('Don\'t take care of history')
        history_accept_text = QCheckBox('Save full text')
        history_accept_text.setToolTip('Save search with more than one words')
        history_box.addWidget(history_overide_btn)
        history_box.addWidget(history_accept_text)
        history_grp.setLayout(history_box)

        vbox.addWidget(spell_check_grp)
        vbox.addWidget(history_grp)
        self.setLayout(vbox)

        self.btn_list = {
            "spell_check" : spell_check_btn,
            "spell_correct" : auto_correct_btn,
            "hist_override" : history_overide_btn,
            "hist_accept_text" : history_accept_text,
        }

        for btn in self.btn_list.values():
            btn.toggled.connect(self.get_states)

    def get_states(self):

        return {option : btn.isChecked() for option, btn in self.btn_list.items()}

    def set_states(self, save):

        for option, btn in self.btn_list.items():
            btn.setChecked(save[option])
        pass

class WindowConfTab(QWidget):

    SESSION_SAVE = {}

    def __init__(self):
        super().__init__()

        vbox = QVBoxLayout()

        self.quit_on_focus = QCheckBox("Close on focus loss")

        vbox.addWidget(self.quit_on_focus)
        self.setLayout(vbox)

    def get_states(self):

        return {'quit_on_focus' : self.quit_on_focus.isChecked()}

    def set_states(self, save):

        self.quit_on_focus.setChecked(save['quit_on_focus'])







