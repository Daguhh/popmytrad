#!/usr/bin/env python3

# MIT No Attribution
#
# Copyright (c) 2021 Daguhh
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software IS
# furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Daguhh"
__license__ = "MIT-0"
__status__ = "Dev"
__version__ = "0.0.1"

"""
=========
PopMyTrad
=========

Surlignez : traduisez!

A Propos
========

surlignez un mot à la souris, puis lancer le script pour afficher une notification contenant:

- la traduction du mot ( `wordreference <www.wordreference.com>`_ )
- et sa définition ( `LeRobert <https://dictionnaire.lerobert.com>`_ )

Dépendances
===========

- BeautifulSoup (python3-bs4)
- pyqt5 (python3-pyqt5)

"""


def run():

    import sys
    from popmytrad.window import MainWindow
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
