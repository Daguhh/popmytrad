#!/usr/bin/env python3

import subprocess, shutil, re

if shutil.which("aspell") is None:
    print("Please install aspell")

def _gen_repl_func(lang):
    """Generate a function to use with re"""

    def func(matchObj):
        """Correct word with re.sub"""

        word = matchObj.group(0)
        words, _ = get_words_proposal(word, lang, is_auto_correct=True)
        return words[0]

    return func

def auto_correct_misspell(word, lang):

    repl_func = _gen_repl_func(lang)
    word = re.sub('\w+', repl_func, word)

    return word

def get_words_proposal(word, lang, is_auto_correct=False):

    words = [word]
    aspell_error = False

    if len(word.strip().split(' '))  > 1:
        return ' ', None

    try:
        cmd = f"echo {word} | aspell -a -l {lang}"
        p = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        stdout, stderr = p.communicate()
        aspell_out = stdout.decode("utf-8")
        aspell_error = stderr.decode("utf-8")

        word_list = re.findall(f"(?<={word}).*:(.*)", aspell_out)
        words = [x.strip() for x in word_list[0].split(",")]

    except IndexError as e:
        if is_auto_correct:
            print("correct word!, said aspell")

    return words, aspell_error


