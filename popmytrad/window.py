#!/usr/bin/env python3

# MIT No Attribution
#
# Copyright (c) 2021 Daguhh
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software IS
# furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import subprocess
import os
import json
from itertools import cycle

from PyQt5.QtGui import QCursor, QDesktopServices, QStandardItemModel, QStandardItem
from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt,
    QModelIndex,
    QItemSelectionModel,
    QTimer,
    QItemSelectionModel,
)
from PyQt5.QtWidgets import (
    QTextBrowser,
    QMainWindow,
    QWidget,
    QTabWidget,
    QVBoxLayout,
    QApplication,
    QStyleFactory,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QPushButton,
    QCheckBox,
    QLineEdit,
    QListView,
    QListWidgetItem,
    QAbstractItemView,
    QComboBox,
)



# from .parse_cmd_args import get_arguments
from .history import SqliteHistory
from .param_tab import ParamTabs, ChooseTabWdg, OrderNChooseWdg, ChooseModulesWdg, WindowConfTab
from .aspell_check import auto_correct_misspell, get_words_proposal
from .html_template import (
    html_template,
    TITLE,
    BOLD,
    ITALIC,
    LINK,
    LIST,
    LIST_ITEM,
    PARAGRAPH,
)

from .get_config import (
    SESSION_SAVE,
    SESSION_SAVE_FILE,
    DICOs_TAB_WIDGETs,
)

from .dico_tab import dico_tab_instances


#################### Main Window #########################################


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        # self.setWindowFlags(Qt.WindowMinimizeButtonHint)
        self.setStyle(QStyleFactory.create("Fusion"))

        self.setWindowFlag(Qt.FramelessWindowHint)  # no bar

        # make small window appear at mouse position
        self.setMouseTracking(True)
        self.move(QCursor.pos())
        self.resize(350, 270)
        self.setFixedSize(self.width(), self.height())

        # Add window content
        window = QWidget()
        self.setCentralWidget(window)

        box = QVBoxLayout(window)

        main_table_widget = QTabWidget() #MainTabs()
        main_table_widget.setTabPosition(QTabWidget.East)

        box.addWidget(main_table_widget)

        self.quit_on_unfocus = SESSION_SAVE['Window']['quit_on_focus']

        # get word from xclip
        self.word = self.get_word(
            source=SESSION_SAVE['Dictionnary']['source'],
            is_correct=SESSION_SAVE['Modules']['spell_correct']
        )

        self.param_tab_widget = ParamTabs(SESSION_SAVE)
        main_table_widget.addTab(self.param_tab_widget, 'Params')

        self.hist_tab = HistoryWdg(self.word_changed_clbk)
        main_table_widget.addTab(self.hist_tab, "Hist")

        self.spell_tab = CheckSpellWdg(self.word, self.word_changed_clbk)
        main_table_widget.addTab(self.spell_tab, "Spell")

        self.dicts_tab = DictTab(self.word, self)
        main_table_widget.insertTab(0,self.dicts_tab, 'Dict')

        main_table_widget.setCurrentIndex(0)

        main_table_widget.currentChanged.connect(self.update_tab_n)

    def word_changed_clbk(self, word):

        self.word = word

    def update_tab_n(self, ind):

        session = self.param_tab_widget.get_states()
        source = session['Dictionnary']['source']
        target = session['Dictionnary']['target']
        self.quit_on_unfocus = session['Window']['quit_on_focus']

        if ind == 0:
            self.dicts_tab.update(self.word)

        elif ind == 1 : # Param
            pass

        elif ind == 2 : #History
            self.hist_tab.populate_hist(self.word, source, target)
            #with SqliteHistory(source, target, self.word) as hist_db:
            #    self.hist_tab.populate_hist(hist_db.get_word_list(), source, target)

        elif ind == 3 :# splell
            self.spell_tab.gen_words_proposal(self.word, source)

    def get_word(self, source, is_correct):
        """ get highligthed word with xclip """

        try:

            word = subprocess.check_output(("xclip", "-o")).decode("utf-8")
            word = word.strip()
            word = word.lower()
            if is_correct:
                word = auto_correct_misspell(word, source)

            return word

        except subprocess.CalledProcessError as e:

            print(e)
            print("please highlight text with mouse before launching the script")

            raise e

    #@staticmethod
    def save_session(self):

        print("===== save sessions ====")

        json.dump(self.param_tab_widget.get_states(), open(SESSION_SAVE_FILE, 'w'))

    def quit(self):

        self.save_session()
        QApplication.quit()

    def keyPressEvent(self, e):
        """Quit when press **q** or **escape**"""

        if e.key() == Qt.Key_Escape:
            self.quit()
        elif e.key() == Qt.Key_Q:
            self.quit()

    def leaveEvent(self, e):
        """Quit when lose focus"""

        if not self.quit_on_unfocus:
            return

        size = self.frameSize()
        pos = self.pos()
        mouse_pos = QCursor.pos()

        if not (pos.x() < mouse_pos.x() < pos.x() + size.width()):
            self.quit()
        if not (pos.y() < mouse_pos.y() < pos.y() + size.height()):
            self.quit()


class MainTabs(QTabWidget):
    """Multiples dictionaries into multiples tables

    Recive word, send it to widgets, manage database
    """

    def __init__(self, parent=None):
        super(MainTabs, self).__init__(parent)

        self.tabBarClicked.connect(self.update)

    def update(self, err):
        pass


def create_fake_dico(Engine, datas):
    """Create a fake dictionary

    The fake dict doesn't perform any search,
    but store and return saved search results

    Parameters
    ----------
    Engine : dico.Dico
        the dico class to mimic
    datas : dict
        datas from history (format is specific to Engine)

    Returns
    -------
    DicoHist(dico.Dico)
        Fake dico class
    """

    datas = datas

    class DicoHist(Engine):

        name = Engine.name
        url = Engine.url

        def __init__(self, source, target):
            super().__init__(source, target)

            self._datas = datas

        @property
        def datas(self):
            return self._datas

        @datas.setter
        def datas(self, datas):
            self._datas = datas

        def get_datas(self, *_):
            return datas

    return DicoHist


class CheckSpellWdg(QTextBrowser):
    """Show a list of word correction proposal"""

    name = "aspell_tab"

    def __init__(self, word, word_change_send):

        super().__init__()

        self.word_change_send = word_change_send

        self.setFontPointSize(10)
        self.setOpenLinks(False)
        self.anchorClicked.connect(self.load_correct_word)

    def gen_words_proposal(self, word, source=None):

        words, aspell_error = get_words_proposal(word, source)

        # Fill with words in history and  make them clickable
        html = TITLE.format(f"Did you mean?    {source}")

        if aspell_error:
            html += PARAGRAPH.format(aspell_error)

        items = ""

        for word in words:
            items += LIST_ITEM.format(LINK.format("aspell:" + word, word))

        html += LIST.format(items)

        self.setHtml(html)

    def load_correct_word(self, url):

        word = url.toString().split(":")[1]
        self.word_change_send(word)


#SORT_ORDER_VALUES = cycle([" Ascending", " Descending"])
SORT_ORDER_VALUES = cycle([' Desc', ' Asc'])
class HistoryWdg(QWidget):

    def __init__(self, word_change_send):

        super().__init__()

        box = QVBoxLayout()

        hbox = QHBoxLayout()

        lbl = QLabel('Sort : ')
        self.sort_criteria_Cb = QComboBox()
        self.sort_criteria_Cb.addItems(['Access Time', 'Alphabetic'])
        self.sort_criteria_Cb.text = self.sort_criteria_Cb.itemText(0)
        self.sort_criteria_Cb.currentIndexChanged.connect(self.change_sort_criteria)
        self.sort_order_Btn = QPushButton(next(SORT_ORDER_VALUES))
        self.sort_order_Btn.clicked.connect(self.change_sort_order)

        hbox.addWidget(lbl)
        hbox.addWidget(self.sort_criteria_Cb)
        hbox.addWidget(self.sort_order_Btn)

        self.tb = HistoryWdgText(word_change_send)

        box.addLayout(hbox)
        box.addWidget(self.tb)

        self.setLayout(box)

    def change_sort_order(self):

        self.sort_order_Btn.setText(next(SORT_ORDER_VALUES))
        self.update_hist_view()

    def change_sort_criteria(self, i):

        self.sort_criteria_Cb.text = self.sort_criteria_Cb.itemText(i)
        self.update_hist_view()

    def populate_hist(self, word, source, target):

        self.word = word
        self.source = source
        self.target = target
        self.update_hist_view()

    def update_hist_view(self):

        sort_criteria = self.sort_criteria_Cb.text
        sort_order = self.sort_order_Btn.text()

        self.tb.populate_hist(self.word, self.source, self.target, sort_criteria, sort_order)

    def load_history_word(self, url):
        self.tb.load_history_word(url)

class HistoryWdgText(QTextBrowser):
    """Show list of previous search in QTextEdit"""

    name = "history_tab"

    def __init__(self, word_change_send):
        super().__init__()

        self.word_change_send = word_change_send

        self.setFontPointSize(10)
        self.setOpenLinks(False)
        self.anchorClicked.connect(self.load_history_word)

    def populate_hist(self, word, source, target, sort_criteria, sort_order):

        with SqliteHistory(source, target, word) as hist_db:
            words = hist_db.get_word_list(sort_criteria, sort_order)

        # Fill with words in history and  make them clickable
        html = TITLE.format(f"historique {source}-{target}")

        items = ""

        for word in words:
            items += LIST_ITEM.format(LINK.format("history:" + word, word))

        html += LIST.format(items)

        self.setHtml(html)

    def load_history_word(self, url):
        """load history word from "history" fake url (history:word)"""

        word = url.toString().split(":")[1]
        self.word_change_send(word)


class DictTab(QTabWidget):
    """Multiples dictionaries into multiples tables

    Recive word, send it to widgets, manage database
    """

    def __init__(self, word, parent):
        super(DictTab, self).__init__(parent)

        self.parent = parent
        self.update(word)

    def update(self, word):

        while self.count():
            self.removeTab(0)

        session = self.parent.param_tab_widget.get_states()
        dicos = session['Dictionnary']['dicos']
        source = session['Dictionnary']['source']
        target = session['Dictionnary']['target']

        category = "word" if len(word.strip().split(' '))  == 1 else "text"

        self.setElideMode(Qt.ElideRight)
        self.setFocusPolicy(Qt.StrongFocus)

        with SqliteHistory(source, target, word) as hist_db:

            for i, (name, is_show) in enumerate(dicos.items()):

                if not is_show:
                    continue

                dicoTabWdg = DICOs_TAB_WIDGETs[name] # get dico class

                if dicoTabWdg.category == "word" and category == "text":
                    dicoTabWdg = create_fake_dico(dicoTabWdg, {'Error': [{'s_wrd': 1}], 'Search for word only': [{'s_wrd': word}]})

                elif hist_db.exist_in(name) and not session['Modules']['hist_override']:
                    datas = hist_db.get_datas_from(name)
                    dicoTabWdg = create_fake_dico(dicoTabWdg, datas)

                else:
                    print(50*'-', '\n', f"Recherche sur {name}...")

                # Create tab
                dico_tab = dicoTabWdg(source, target)
                self.addTab(dico_tab, dicoTabWdg.name[:4])
                self.setTabToolTip(i, dicoTabWdg.name)
                dico_tab.translate_n_show(word)

                if category == "text" and not session['Modules']['hist_accept_text'] :
                    continue

                if not hist_db.exist_in(dicoTabWdg.name):
                    hist_db.add(dicoTabWdg.name, dico_tab.datas)

                if session['Modules']['hist_override']:
                    hist_db.update(dicoTabWdg.name, dico_tab.datas)


    def give_focus(self, index):
        self.setCurrentIndex(index)


if __name__ == "__main__":

    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
