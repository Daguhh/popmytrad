#!/usr/bin/env python3

import sys, json, os, shutil

from dico import DICOs
from .dico_tab import gen_tabs_class

default_config = r"""{
  "Dictionnary": {
    "source": "fr",
    "target": "en",
    "dicos": {}
  },
  "Modules": {
    "spell_tab": false,
    "spell_check": true,
    "spell_correct": false,
    "hist_tab": false,
    "hist_override": false,
    "hist_accept_text": false
  },
  "Window": {
    "quit_on_focus": true
  }
}"""

SESSION_SAVE_FILE = os.path.join(os.getenv("HOME"), ".popmytrad/session.json")
DICOs_TAB_WIDGETs = {d.name:gen_tabs_class(d) for d in DICOs}

# Load previous session
try:
    SESSION_SAVE = json.load(open(SESSION_SAVE_FILE, "r"))
except FileNotFoundError as err:
    print("""No previous sessions, generating config...""")
    SESSION_SAVE = json.loads(default_config)

# Add dict that don't appear in session save
SESSION_SAVE['Dictionnary']['dicos'].update({
    d.name : 0
    for d in DICOs
    if not d.name in SESSION_SAVE['Dictionnary']['dicos']
})

