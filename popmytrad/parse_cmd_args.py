import argparse


def get_arguments():

    parser = argparse.ArgumentParser(
        description="""Traduit ou définit le mot surligné""",
        epilog="""
            Par défaut, toutes les sources web sont questionnées.
            Pour restreindre la recherche à seulement quelques unes d’entre elles,
            précisez en arguments toutes les sources souhaités""",
    )

    parser.add_argument(
        "-e",
        "--engine",
        help="activate seach engine and its display in a tab, by default all engines are used (comma separated list)",
        dest="engines",
        default="session",
    )
    parser.add_argument(
        "-l",
        "--list-engines",
        help="show translation/defintion engines available",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--source",
        help="source language, default='fr'",
        dest="source",
        default="session",
    )
    parser.add_argument(
        "-t",
        "--target",
        help="targeted language, default='en'",
        dest="target",
        default="session",
    )
    parser.add_argument(
        "-a",
        "--spell-check",
        help="auto correct mispell word, given source language (guess with aspell",
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--configure",
        help="display config tab only (dicts and lang)",
        action="store_true"
    )
    parser.add_argument(
        "-o",
        "--overide-history",
        help="ne pas tenir compte de l'historique et y remplacer la recherche",
        action="store_true",
    )

    args, unknown = parser.parse_known_args()

    return args
