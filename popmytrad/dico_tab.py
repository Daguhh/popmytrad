from PyQt5.QtGui import QDesktopServices, QCursor
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTextBrowser, QApplication, QAction, QMenu

from .html_template import html_template

dico_tab_instances = []


def gen_tabs_class(DicoClass):
    class DicoTab(DicoClass, QTextBrowser):
        """ A base class for dictionnary """

        def __init__(self, source, target):

            super().__init__(source=source, target=target)

            self.setFontPointSize(8)
            self.setOpenLinks(False)
            self.createStandardContextMenu()

            self.anchorClicked.connect(self.open_url)

            self.setContextMenuPolicy(Qt.CustomContextMenu)
            self.customContextMenuRequested.connect(self.custom_context_menu)
            self.add_custom_context_menu()

            global dico_tab_instances
            dico_tab_instances += [self]

        def add_custom_context_menu(self):

            copy = QAction(text="copy selection", parent=self)
            copy.setShortcut("Ctrl+c")
            copy.triggered.connect(self.copy_selected_text)

            copy_all = QAction(text="copy (plain text)", parent=self)
            copy_all.setShortcut("Ctrl+p")
            copy_all.triggered.connect(self.copy_all)

            copy_html = QAction(text="copy (html)", parent=self)
            copy_html.setShortcut("Ctrl+h")
            copy_html.triggered.connect(self.copy_html)

            copy_json = QAction(text="copy (json)", parent=self)
            copy_json.setShortcut("Ctrl+j")
            copy_json.triggered.connect(self.copy_json)

            self.custom_menu = QMenu()
            self.custom_menu.addAction(copy)
            self.custom_menu.addAction(copy_all)
            self.custom_menu.addAction(copy_json)
            self.custom_menu.addAction(copy_html)

        def custom_context_menu(self, event):

            self.custom_menu.popup(QCursor.pos())

        def copy_selected_text(self, event=None):
            print("copy select")
            selected_text = self.textCursor().selectedText()
            QApplication.clipboard().setText(selected_text)

        def copy_all(self, event=None):
            print("copy all")
            text = self.toPlainText()
            QApplication.clipboard().setText(text)

        def copy_html(self, event=None):
            print("copy html")
            text = self.toHtml()
            QApplication.clipboard().setText(text)

        def copy_json(self, event=None):
            print("copy json")
            import json

            text = json.dumps(self.datas, indent=4)
            QApplication.clipboard().setText(text)

        def keyPressEvent(self, e):

            if e.key() == Qt.Key_C and e.modifiers() == Qt.ControlModifier:
                self.copy_selected_text()
            elif e.key() == Qt.Key_P and e.modifiers() == Qt.ControlModifier:
                self.copy_all()
            elif e.key() == Qt.Key_H and e.modifiers() == Qt.ControlModifier:
                self.copy_html()
            elif e.key() == Qt.Key_J and e.modifiers() == Qt.ControlModifier:
                self.copy_json()

        def open_url(self, url):

            print(f"Going to {url}")
            if url.scheme() == "https" or url.scheme() == "http":
                QDesktopServices.openUrl(url)

        def translate_n_show(self, word):
            """display word translation (or def) in QTextEdit"""

            self.translate(word)
            html = self.get_html(word, self.datas)
            self.render_html(html)

        def render_html(self, html):
            """Send html text to widget"""

            self.setHtml(html_template.format(html))

    return DicoTab
